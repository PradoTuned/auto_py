class DatalogLineObject(object):
    ''' A class used for holding each key/value pair for a single datalog line'''

    def StoreValue(self, name_of_prop=None, value=None):
        '''Dyanamically store each key/value pair into this DatalogLineObject'''
        try:
            setattr(self, name_of_prop, int(value))
        except:
            setattr(self, name_of_prop, float(value))
