import csv
from . import DatalogLineObject


class DatalogReader(object):
    """Class to deal with DataLogLine objects for extracting required information from the datalog line."""

    CONST_LAMBDA = "lambda"
    CONST_TARGET_LAMBDA = "targetlambda"
    CONST_RPM = "rpm"
    CONST_TPS = "tps"

    def __init__(self, csv_file):
        self.datalog_objects = []
        self.proper_datalog_csv = []
        self.current_csv_file_path = csv_file

        # Call this function for every datalogreader object created
        self.remove_unneeded_parameters()
        self.parse_datalog_lines()

    def parse_datalog_lines(self):
        """Store each datalog key value pair for ease of information extraction"""

        for line in self.proper_datalog_csv:
            data_object = DatalogLineObject.DatalogLineObject()
            for currentitem in line:
                data_object.StoreValue(currentitem, line[currentitem])
            self.datalog_objects.append(data_object)

    def remove_unneeded_parameters(self):
        """Allows easier management of datalog line objects"""
        with open(self.current_csv_file_path) as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for line in csv_reader:
                del line["frame"]
                del line["clv"]
                del line["camangle"]
                del line["targetcamangle"]
                del line["injector"]
                del line["ignition"]
                del line["ta"]
                del line["tw"]
                del line["shortterm"]
                del line["longterm"]
                del line["knockcount"]
                del line["voltage"]
                del line["eld.v"]
                del line["eld"]
                del line["s02"]
                del line["brake"]
                del line["gear"]
                del line["boostduty"]
                del line["analog0"]
                del line["analog1"]
                del line["analog2"]
                del line["analog3"]
                del line["analog4"]
                del line["analog5"]
                del line["analog6"]
                del line["analog7"]
                del line["digital0"]
                del line["digital1"]
                del line["digital2"]
                del line["digital3"]
                del line["digital4"]
                del line["digital5"]
                del line["digital6"]
                del line["digital7"]
                self.proper_datalog_csv.append(line)

    def retrieve_datalog_lines(
        self, name_of_par=None, par_limit=0, par_limit_deadband=0
    ):
        temp_datalog_objects = []
        for cur_datalog_object in self.datalog_objects:
            if self.verify_if_within_limits(
                cur_datalog_object, name_of_par, par_limit, par_limit_deadband
            ):
                temp_datalog_objects.append(cur_datalog_object)

        return temp_datalog_objects

    def retrieve_datalog_lines2(
        self, name_of_par=None, par_limit=0, par_limit_deadband=0
    ):
        """Return requested datalog line objects based on passed in criteria"""
        temp_datalog_objects = []
        for cur_datalog_object in self.datalog_objects:
            is_within_limits = True
            for (cur_name, cur_par_lim, cur_par_lim_deadband) in zip(
                name_of_par, par_limit, par_limit_deadband
            ):
                if not self.verify_if_within_limits(
                    cur_datalog_object, cur_name, cur_par_lim, cur_par_lim_deadband,
                ):
                    is_within_limits = False

            if is_within_limits:
                temp_datalog_objects.append(cur_datalog_object)

        return temp_datalog_objects

    def verify_if_within_limits(
        self, cur_datalog_object, cur_name, cur_par_lim, cur_par_lim_deadband
    ):
        """To allow for extraction of the datalog object if it meets the criteria"""
        if getattr(cur_datalog_object, cur_name) >= (
            cur_par_lim - (cur_par_lim * cur_par_lim_deadband)
        ) and getattr(cur_datalog_object, cur_name) <= (
            cur_par_lim + (cur_par_lim * cur_par_lim_deadband)
        ):
            return True
        return False

    # TODO: delete this method as I think it's no longer needed.
    @staticmethod
    def parse_retrieved_datalog_lines(datalog_lines, target_lambdas_to_parse_for):
        for current_target_lambda in target_lambdas_to_parse_for:
            extracted_vals = list(
                cur_datalog_line
                for cur_datalog_line in (
                    filter(
                        lambda cur_datalog_line: getattr(
                            cur_datalog_line, DatalogReader.CONST_TARGET_LAMBDA
                        )
                        == current_target_lambda,
                        datalog_lines,
                    )
                )
            )
            print("Hello")

    # TODO: finish off method for iterating through retrieved datalog lines
    """
    For example, method could iterate through the datalog lines, starting from the first
    (pretending to be looking at the datalog graph would be the left side of the section) and start
    taking notes of quad cells? for percent different in lambda to targetlambda. or.... we do row
    percentage manipulation. Need to be able to handle the case where rows overlap so we don't over compensate.
    """
