import numpy as np
from corealgorithms import tuning_utility as utl
import array
import re

# Class that will hold each "table" to be tuned.
# Contains the datastrucutre TableOfValues that is a 3D array of RPM vs. Load VS Cam Angle


class TuningTable(object):
    CONST_CAM = "Cam"
    CONST_ROW = "Row"
    CONST_NUM_OF_LINES = 129
    CONST_NUM_OF_LINES_SHORT = 60
    CONST_NUM_OF_LINES_FOR_SINGLE_CAM_ANGLE = 29

    # Default Constructor for the TuningTable class
    def __init__(self):
        self.TableNumber = -1
        self.Name = None
        self.Rows = -1
        self.Cols = -1
        self.NumCamTables = -1
        self.HighSpeed = -1
        self.LiveTuningEnabled = []
        self.Rpm = []
        self.Load = []
        self.table_of_values = np.zeros((0, 0, 0))

    # TODO: Insert comment for explaining this crucial method
    """
    This is the main function that extracts data line by line
    and begins to store it into the different properties/table of the TuningTable
    """

    def extract_val(self, lines_of_data_for_table=None):
        if self.validate_data_lines_for_table(lines_of_data_for_table) == False:
            return

        for line_item in lines_of_data_for_table:
            if line_item.startswith("[Table"):
                continue
            self.adjust_datalines_for_single_cam_table(lines_of_data_for_table)

            key_of_line, val_of_line = line_item.split("=")

            if TuningTable.check_if_list(val_of_line):
                if TuningTable.check_if_cam_val(key_of_line):
                    self.store_into_table_of_values(key_of_line, val_of_line)
                else:
                    self.store_val_into_list_prop(key_of_line, val_of_line)
            else:
                if key_of_line.startswith("Name"):
                    setattr(self, key_of_line, val_of_line)
                else:
                    setattr(self, key_of_line, int(val_of_line))

    def adjust_datalines_for_single_cam_table(self, item_to_extract):
        if int(self.NumCamTables) > 0:
            if int(self.NumCamTables) == 1:
                if (
                    len(item_to_extract) == TuningTable.CONST_NUM_OF_LINES
                    or len(item_to_extract) == TuningTable.CONST_NUM_OF_LINES_SHORT
                    or len(item_to_extract)
                    == TuningTable.CONST_NUM_OF_LINES_FOR_SINGLE_CAM_ANGLE + 1
                ):
                    # Delete all items to the end of the list
                    del item_to_extract[
                        TuningTable.CONST_NUM_OF_LINES_FOR_SINGLE_CAM_ANGLE:
                    ]

    def validate_data_lines_for_table(self, item_to_extract):
        # Grab the first item in the list, and extract the first digit (store it as the Table Number - self.TableNumber)
        self.TableNumber = utl.get_first_item(
            TuningTable.extract_indices(item_to_extract[0])
        )

        # if TuningTable.extract_indices couldn't pull a proper number, then it will fail and return False
        if self.TableNumber == -1 and item_to_extract == None:
            return False

    def store_val_into_list_prop(self, prop_name=None, val_to_store=None):
        if prop_name == None or val_to_store == None:
            pass

        # TODO: Insert code to get the property of self via the propertyName parameter
        # properyList =
        temp_list_of_vals = utl.convert_csv_line_to_list(val_to_store)
        # this handles the issue between going from LiveTuningEnable and RPM to Load (where it's an int vs float for Load)
        # This catches the exception, swallows it, and stores the values as floats
        prop_list = []
        for item in temp_list_of_vals:
            try:
                prop_list.append(int(item))
            except:
                prop_list.append(float(item))

        setattr(self, prop_name, prop_list)

    # Main method that takes the table values and stores them in their respective position
    # in the 3D array
    # This method is considered an instance method (because it's not tagged with @classmethod or @staticmethod)
    def store_into_table_of_values(self, table_index_to_extract, list_with_vals=None):
        if table_index_to_extract == None:
            pass

        indices_to_use = TuningTable.extract_indices(table_index_to_extract)

        list_with_separated_vals = utl.convert_csv_line_to_list(list_with_vals)

        if self.Cols > 0 and self.Rows > 0:
            if self.table_of_values.shape == (0, 0, 0):
                self.table_of_values = np.zeros(
                    (self.Rows, self.Cols, self.NumCamTables)
                )
            for i, item in enumerate(list_with_separated_vals, 1):
                self.table_of_values[
                    indices_to_use[1], i - 1, indices_to_use[0]
                ] = float(item)

    # Pass in required DatalogLine object (to access the ".rpm" property)
    def find_rows(self, datalog_lines):
        rows_to_return = []
        for cur_datalog_line in datalog_lines:
            closest_rpms = self.find_nearest_rpms(cur_datalog_line.rpm)
            for cur_rpm in closest_rpms:
                rows_to_return.append(cur_rpm)

        rows_to_return = list(dict.fromkeys(rows_to_return))
        rows_to_return.sort()

        return rows_to_return

    # Returns the indices for the quad cells to use for searching the 3D table
    def find_nearest_quad_cells(self, current_rpm, current_load):
        closest_rpms = self.find_nearest_rpms(current_rpm)
        closest_loads = self.find_nearest_loads(current_load)

        indices_of_rpms = list(self.Rpm.index(i) for i in closest_rpms)
        indices_of_load = list(self.Load.index(i) for i in closest_loads)
        """
        Need to find Two rpms closest to the "current item"

        Just need to decide if we are doing direct tuning table access at the lower
        level find_nearest_XXXX methods or not

        to find the 2 closest, we just need to check if
        """

        return np.array([indices_of_rpms, indices_of_load])

    def find_index_by_prop(self, prop_name, val_to_check):
        return (getattr(self, prop_name)).index(val_to_check)

    # Returns raw rpm values to be used for finding index in table
    def find_nearest_rpms(self, current_rpm):
        return utl.find_closest_2_nums(self.Rpm, current_rpm)

    # Returns raw load values to be used for finding index in table
    def find_nearest_loads(self, current_load):
        return utl.find_closest_2_nums(self.Load, current_load)

    # Helper function to parse string and extract the indices based on the value passed in
    @staticmethod
    def extract_indices(val_to_extract_from):
        if val_to_extract_from == None:
            pass
        else:
            indices_to_return = []
            if val_to_extract_from.startswith("[Table"):
                indices_to_return.append(
                    int(val_to_extract_from.split("e")[1].split("]")[0])
                )
            else:
                # from here: https://www.geeksforgeeks.org/python-extract-digits-from-given-string/
                # using join() + isdigit() + filter()
                # Extract digit string
                # I modified it to create it into a list, but also iterate through the iterable "filter" object
                # and converted each item (i) into an int as I iterated through the filter object
                # inline for statement
                # indices_to_return = list(int(i) for i in (
                # filter(lambda i: i.isdigit(), val_to_extract_from)))
                # from here: https: // stackoverflow.com/a/4289348
                # regex to find all digits, and convert to list with inline for statement
                indices_to_return = list(
                    int(i) for i in re.findall(r"\d+", val_to_extract_from)
                )

            return indices_to_return

    # Helper static function to check if the value starts with "Cam"
    @staticmethod
    def check_if_cam_val(val_to_check=""):
        return val_to_check.startswith(TuningTable.CONST_CAM)

    # Helper static function to check if the passed in line is a list
    # (based on pre-determined value of containig commas)
    @staticmethod
    def check_if_list(item_to_check_if_comma_separated_list=None):
        if item_to_check_if_comma_separated_list == None:
            return False
        else:
            return "," in str(item_to_check_if_comma_separated_list)

    # TODO: create method to extract all target lambdas for each cam angle
    # into a single line list for ease of use?
