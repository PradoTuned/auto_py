import PySimpleGUI as sg
# from ..test_files import Sandbox as sb
import simplegui

kalFileKey = "KalFile"
datalogFileKey = "DLFile"
theme = "Black"
win_name = "SimpleGui"
resizable = True
layout = [
    [sg.Text("Select Kal File")],
    [sg.Input(key=kalFileKey), sg.FileBrowse(file_types=[".kal"])],
    [sg.Text("Select Kal File")],
    [sg.Input(key=datalogFileKey), sg.FileBrowse(file_types=[".csv"])],
    [sg.OK(button_text="Load"), sg.Cancel()],
]


def show_window(simplegui_obj):
    while True:
        event, values = simplegui_obj.window.read()
        # TODO Need to fix this "cancel" to be something that was passed in
        if event in (None, "Cancel"):
            break
        if event == "Load":
            return values
            # return [values["KalFile"], values["DatalogFile"]]
    simplegui_obj.window.close()


mySimpleGui = simplegui.SimpleGui(theme, win_name, layout, resizable)
myList = show_window(mySimpleGui)
sg.popup(myList)


'''
sg.theme("Black")

layout = [
    [sg.Text("Select Kal File")],
    [sg.Input(key="KalFile"), sg.FileBrowse(file_types=[".kal"])],
    [sg.Text("Select Kal File")],
    [sg.Input(key="DatalogFile"), sg.FileBrowse(file_types=[".csv"])],
    [sg.OK(button_text="Load"), sg.Cancel()],
]

window = sg.Window("AutoTuner", layout)
window.Resizable = True

while True:
    event, values = window.read()
    if event in (None, "Cancel"):
        break
    if event == "OK":
        pass# sb.myFunc(values["KalFile"], values["DatalogFile"])
    print(event, values)
window.close()
'''
