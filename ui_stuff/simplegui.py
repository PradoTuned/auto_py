import PySimpleGUI as sg


class SimpleGui(object):
    def __init__(self, theme=None, win_name=None, layout=None, resizable=None):
        if not theme:
            theme = "Black"

        if not win_name:
            win_name = "DefaultTitle"

        if not layout:
            layout = [
                [sg.Text("Default Layout")]
            ]

        if not resizable:
            resizable = False

        self.theme = theme
        self.win_name = win_name
        self.layout = layout
        self.resizable = resizable

        sg.theme(self.theme)
        self.window = sg.Window(self.win_name, self.layout)
        self.window.Resizable = self.resizable
