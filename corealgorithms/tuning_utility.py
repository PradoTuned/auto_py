# import DetectDecodingErrorsLine
import io
import copy
import enum
from bisect import bisect_left
from statistics import mean
import numpy as np


def fix_kal_file_temperature_symbols():
    print("Reading file....")
    """
    For replacing lines of code within a line: https://stackoverflow.com/questions/4719438/editing-specific-line-in-text-file-in-python
    
    For replacing items in a line for a text file https://stackoverflow.com/questions/16916013/replacing-an-item-in-a-python-list-by-index-failing#16916033
    
    Open up the file as read-only with required parameters to ignore errors
    """
    with io.open(
        "/Users/hector/Projects/Active/auto_py/testFile.kal",
        encoding="utf-8",
        errors="surrogateescape",
    ) as calFile:
        # Store the file into a list by using .readlines() so that we can do line-by-line manipulation
        tempCalFile = calFile.readlines()
        # Iterate through the lines using the enumerate option to be able to use both i index, and line variables
        for i, line in enumerate(tempCalFile, 1):
            # Function returns the column for which an error exists on the current line "line"
            currentError = DetectDecodingErrorsLine.detect_decoding_errors_line(
                line)
            if currentError:
                # Separate the line into a list of characters via the list() function
                lineAsList = list(tempCalFile[i - 1])
                print(f"Found errors on line {i}:")
                for (col, b) in currentError:
                    print(f" {col + 1:2d}: {b[0]:02x}")
                    # Update the error via the found "col" variable on the character list created above
                    lineAsList[col] = "°"

                # Using "".join() function allows us to append all characters in the list into a single string (vs list of chars)
                tempCalFile[i - 1] = "".join(lineAsList)

        # Re-open the file in "Write" mode via the "w" parameter so that we can re-write the file out (we cannot, or should not, directly modify the file when read-in)
        with io.open(
            "/Users/hector/Projects/Active/auto_py/testFile.kal",
            "w",
            encoding="utf-8",
            errors="surrogateescape",
        ) as calFile:
            calFile.writelines(tempCalFile)


def convert_csv_line_to_list(csv_line_to_convert=None):
    """Return a list object with each value as an index in the list"""
    if csv_line_to_convert == None:
        return

    line_of_csv = csv_line_to_convert.split(",")
    return list(line_of_csv)


def get_first_item(list_to_get_first_item_from):
    """Always returns first item

    TODO: Need to add exception handling if the list is empty
    """
    if list_to_get_first_item_from:
        return list_to_get_first_item_from[0]


def find_average_lambda_diff(list_of_lambdas):
    return round(mean(list_of_lambdas), 2)


def lambda_percent_diff(curr_lambda, target_lambda):
    return 1 - (target_lambda / curr_lambda)


# Assume item_to_multiple is a list(or numpy array?)
# Assume dec_to_mult_by is percentage in decimal form


def multiply_numpy_arr_by_percentage(items_to_multiply, dec_to_mult_by):
    """
    Simple function to multiply a passed in array by a percentage (as decimal)

    Numpy array is passed by reference, thus no return val.
    """
    for item in np.nditer(items_to_multiply, op_flags=["readwrite"]):
        item[...] = item + (item * dec_to_mult_by)


# Code was taken from this answer based on quickest execution time
# https://stackoverflow.com/a/12141511
def find_closest_2_nums(current_list, number_to_search_for):
    """
    Assumes current_list is sorted. Returns closest value to number_to_search_for

    If 2 numbers are equally close, return the smallest number.
    """
    pos = bisect_left(current_list, number_to_search_for)
    if pos == 0:
        return [current_list[0], current_list[1]]
    if pos == len(current_list):
        return [current_list[-2], current_list[-1]]
    current_found = current_list[pos]
    if current_found > number_to_search_for:
        return [current_list[pos - 1], current_found]
    else:
        return [current_found, current_list[pos + 1]]


def return_class_props(class_to_use):
    return list(class_to_use.__dict__.keys())

# PROBABLY UNNEEDED CODE HERE:
# This function is probably not even needed based on the kal
# # file's map "reading" already
# def convert_map_reading(map_value):
#     mBar_conversion_factor = 1000
#     return map_value * mBar_conversion_factor  # TODO: Figure out conversion factor


# def convert_lambda_to_afr(lambda_val, fuel_type=FuelType.Gasoline):
#     return lambda_val * fuel_type  # TODO: Figure out the approp. conv. factor


# class FuelType (enum):
#     Gasoline = 14.7
#     Ethanol = 9
#     E85 = 9
#     Methanol = 6.4

#     def __str__(self):
#         return self.value
