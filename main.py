import datalogging.TuningTable as TuningTable
import io
import csv
import datalogging.DatalogReader as DatalogReader
from corealgorithms import tuning_utility as utl
from ui_stuff import simplegui
import PySimpleGUI as sg
from time import sleep
import json


def create_list_of_tuning_tables(cal_file):
    '''TODO Keep this code for later for using TuningTable'''
    list_of_tables = []
    for i in range(0, 18):
        current_tuning_table = TuningTable.TuningTable()

        index_of_table = list(cal_file).index("[Table%d]\n" % i)

        srrng_lines_for_current_table = list(cal_file)[
            index_of_table: index_of_table + current_tuning_table.CONST_NUM_OF_LINES
        ]

        current_tuning_table.extract_val(list(srrng_lines_for_current_table))

        list_of_tables.append(current_tuning_table)

    return list_of_tables


kal_file_key = "-KALFILE-"
datalog_file_key = "-DLFILE-"


def create_load_window():
    theme = "Black"
    win_name = "Load Files"
    resizable = True
    layout = [
        [sg.Text("Select Kal File")],
        [sg.Input(key=kal_file_key), sg.FileBrowse(file_types=[".kal"])],
        [sg.Text("Select Kal File")],
        [sg.Input(key=datalog_file_key), sg.FileBrowse(file_types=[".csv"])],
        [sg.OK(button_text="Load"), sg.Cancel()],
    ]
    return simplegui.SimpleGui(theme, win_name, layout, resizable)


def show_loadwindow_and_return_file_paths(simplegui_obj):
    while True:
        event, values = simplegui_obj.window.read(timeout=10)
        # TODO Need to fix this "cancel" to be something that was passed in
        if event in (None, "Cancel"):
            break
        if event == "Load":
            simplegui_obj.window.hide()
            return values
    simplegui_obj.window.close()


'''
TODO: Need to be able to dynamically add in new items to the window?
For example, we select a table
then, we have rows of selectable/typable parameters:
    First row will be first set of parameters?
    or maybe when the OK button is clicked, it will store it into a list?
    or a + or add button and a new row of the same GUI elements are added.
    
    Easier way would be if when the OK button is clicked, it stores the 3 parameters into a list,
    clears out the selections, and allows the user to re-add more parameters.
    
    Need to add in another button so we can finalize our choices.
'''


def create_tuning_window(lb_tables, lb_params):
    theme = "Black"
    win_name = "Tuning Window"
    resizable = True
    layout = [
        [sg.Listbox(key="-TABLESKEY-", values=lb_tables, size=(30, int(len(lb_tables)/2)),
                    auto_size_text=True, background_color="Black", text_color="White", enable_events=True)],
        [sg.Listbox(key="-PARAMSKEY-", values=lb_params, size=(30, int(len(lb_params))),
                    auto_size_text=True, background_color="Black", text_color="White", enable_events=True), sg.In(key="-VALKEY-", default_text="Value", enable_events=True), sg.In(key="-TOLKEY-", default_text="Tolerance", enable_events=True)],
        [sg.Multiline(key="-MULTI-")],
        [sg.OK(),  sg.Cancel()]
    ]
    return simplegui.SimpleGui(theme, win_name, layout, resizable)


tuning_params = {}


def show_tuning_window(simplegui_obj):
    param_counter = 0
    while True:
        event, values = simplegui_obj.window.read(timeout=10)
        # TODO Need to fix this "cancel" to be something that was passed in
        if event in (None, "Cancel"):
            break
        if event == "OK":
            multi = "-MULTI-"
            param_counter = store_into_tuning_params(
                ["Table", values["-TABLESKEY-"][0], "Param", values["-PARAMSKEY-"][0], "Value", values["-VALKEY-"], "Tolerance", values["-TOLKEY-"]], param_counter)
            simplegui_obj.window[multi].print("Table: " + values["-TABLESKEY-"][0] + " Param: " +
                                              values["-PARAMSKEY-"][0] + " Value: " + values["-VALKEY-"] + " Tolerance: " + values["-TOLKEY-"])
            simplegui_obj.window[multi].print(tuning_params[0])
            parse_tuning_params(param_counter)
            sleep(0.5)

    return event, values


'''
Iterate through the tuning_params, selecting items for the same table.
For each table, pick out either:
    all params to pick the datalog lines,
    or pick out params in sections to do subsections of params to filter datalog lines from
'''


def store_into_tuning_params(param_list, param_counter):
    # taken from here: https://stackoverflow.com/questions/4576115/convert-a-list-to-a-dictionary-in-python/4576128#4576128
    temp = iter(param_list)
    tuning_params[param_counter] = dict(zip(temp, temp))
    param_counter += 1
    return param_counter

def parse_tuning_params(index_of_tuning_param):
    '''Parses the global tuning_params dict to gather each param into their respective tuples/lists for later processing'''
    name_param = []
    value_param = []
    
    for key, value in tuning_params[index_of_tuning_param - 1].values:
        name_param.append(key)
        value_param.append(value)

def load_and_return_kal_and_datalog_objects(kal_file_path, datalog_file_path):
    with io.open(kal_file_path, encoding="utf-8", errors="surrogateescape") as calFile:
        temp_cal_file = calFile.readlines()

    list_of_tuning_tables = create_list_of_tuning_tables(temp_cal_file)
    datalog_obj = DatalogReader.DatalogReader(datalog_file_path)
    return [list_of_tuning_tables, datalog_obj]

def process_tuning_params(datalog_obj, *args):
    '''This function assumes *args has the first param at the first index, and second at the second, and so on'''
    datalog_lines = datalog_obj.retrieve_datalog_lines2()
    pass


def main():
    load_file_window = create_load_window()
    file_paths = show_loadwindow_and_return_file_paths(load_file_window)
    if file_paths:
        list_of_tuning_tables, datalog_obj = load_and_return_kal_and_datalog_objects(
            file_paths[kal_file_key], file_paths[datalog_file_key])

    # print(datalog_obj.datalog_objects[0])
    # grab a datalog line object to gather the property names for selection
    tuning_table_names = [item.Name for item in list_of_tuning_tables]
    tuning_window = create_tuning_window(tuning_table_names,
                                         utl.return_class_props(datalog_obj.datalog_objects[0]))
    tuning_event, tuning_vals = show_tuning_window(tuning_window)
    # while True:
    #     event, values = tuning_window.window.read(timeout=10)
    #     if event in (None, "Cancel"):
    #         break
    #     if event == "OK":
    #         process_tuning_params(values)

    print(list_of_tuning_tables, datalog_obj)


if __name__ == "__main__":
    main()
