import datalogging.TuningTable as TuningTable
import io
import csv
import datalogging.DatalogReader as DatalogReader
from corealgorithms.tuning_utility import tuning_utility as utl
from ui_stuff import simplegui
import PySimpleGUI as sg

"""
TODO:
Create a FileManager?
    Perhaps loads in the cal file and datalog file, etc.
Create a TuningTable manager?
    The TuningTableManager or the TuningTable itself will need to keep track of how much each row has been adjusted.
        For example, if rows 1-3 got adjusted 10% positive at the start of the datalog, and then later in the datalog
        there's another adjustment of say 15% required, then we should take the difference and only adjust another 5%.
        Vice versa, if it's already 10% increase, and later on it requires only 5%, we keep the 10% increase.

"""


def CreateListOfTuningTables(cal_file):
    '''TODO Keep this code for later for using TuningTable'''
    list_of_tables = []
    for i in range(0, 18):
        current_tuning_table = TuningTable.TuningTable()

        index_of_table = list(cal_file).index("[Table%d]\n" % i)

        srrng_lines_for_current_table = list(cal_file)[
            index_of_table: index_of_table + current_tuning_table.CONST_NUM_OF_LINES
        ]

        current_tuning_table.extract_val(list(srrng_lines_for_current_table))

        list_of_tables.append(current_tuning_table)

    return list_of_tables


def show_window(simplegui_obj):
    while True:
        event, values = simplegui_obj.window.read()
        # TODO Need to fix this "cancel" to be something that was passed in
        if event in (None, "Cancel"):
            break
        if event == "Load":
            return values
            # return [values["KalFile"], values["DatalogFile"]]
    simplegui_obj.window.close()


def myFunc(kal_file_to_use, datalog_file_to_use):

    # Do Initial read-in of cal file
    # Open up the file as read-only with required parameters to ignore errors
    with io.open(
        # "/Users/hector/Projects/Active/auto_py/TestFiles/testFile.kal",
        kal_file_to_use,
        encoding="utf-8",
        errors="surrogateescape",
    ) as calFile:
        temp_cal_file = calFile.readlines()

    datalogobject = DatalogReader.DatalogReader(
        # "/Users/hector/Projects/Active/auto_py/TestFiles/testDatalog.csv"
        datalog_file_to_use
    )
    datalogobject.parse_datalog_lines()
    my_temp_datalog_lines = datalogobject.retrieve_datalog_lines(
        "tps", 90, 0.2)
    my_temp_datalog_lines2 = datalogobject.retrieve_datalog_lines2(
        ("tps", "targetlambda", "lambda"), (90, 0.85, 0.80), (0.2, 0.1, 0.05)
    )

    my_list_of_Tables = CreateListOfTuningTables(temp_cal_file)
    # = my_list_of_Tables[0].find_nearest_quad_cells(2200, 0.560)
    # my_list_of_raw_rpms = my_list_of_Tables[0].find_rows(my_temp_datalog_lines)
    # my_list_of_rpm_rows = list(my_list_of_Tables[0].find_index_by_prop(
    #     "Rpm", i) for i in my_list_of_raw_rpms)

    # TODO: add in method to TuningTableManager for tuning table to access by name?
    my_list_of_raw_rpms2 = my_list_of_Tables[2].find_rows(
        my_temp_datalog_lines2)
    my_list_of_rpm_rows2 = list(
        my_list_of_Tables[2].find_index_by_prop("Rpm", i) for i in my_list_of_raw_rpms2
    )

    # TODO: Add add in methods for extract values within dataloglineobjects from a datalog reader
    list_of_lambdas = list(getattr(item, "lambda")
                           for item in my_temp_datalog_lines2)
    list_of_lambda_diff = list(
        utl.lambda_percent_diff(item, 0.85) for item in list_of_lambdas
    )
    average_lambda_diff = utl.find_average_lambda_diff(list_of_lambda_diff)

    print(my_list_of_Tables[2].table_of_values[my_list_of_rpm_rows2[0], :, 0])
    utl.multiply_numpy_arr_by_percentage(
        my_list_of_Tables[2].table_of_values[my_list_of_rpm_rows2[0], :, 0],
        average_lambda_diff,
    )
    print(my_list_of_Tables[2].table_of_values[my_list_of_rpm_rows2[0], :, 0])
